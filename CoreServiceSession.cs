using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.Xml;
using Tridion.ContentManager.CoreService.Client;

namespace ContentUploadToSDLTridion
{
    public class CoreServiceSession : IDisposable
    {
        private readonly CoreServiceClient _client;
        private string ServiceUrl = ConfigurationManager.AppSettings["CoreServiceUrl"];
        private const int MaxMessageSize = 10485760;

        public CoreServiceSession(string hostname)
        {
            _client = CreateBasicHttpClient(hostname);
        }

        public CoreServiceSession(string hostname, string username, string password)
        {
            _client = CreateBasicHttpClient(hostname);

            _client.ChannelFactory.Credentials.Windows.ClientCredential =
                new System.Net.NetworkCredential(username, password);
        }

        private CoreServiceClient CreateBasicHttpClient(string hostname)
        {
            var basicHttpBinding = new BasicHttpBinding
            {
                MaxReceivedMessageSize = MaxMessageSize,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = MaxMessageSize,
                    MaxArrayLength = MaxMessageSize
                },
                Security = new BasicHttpSecurity
                {
                    Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                    Transport = new HttpTransportSecurity
                    {
                        ClientCredentialType = HttpClientCredentialType.Windows
                    }
                }
            };
            var remoteAddress = new EndpointAddress(string.Format(ServiceUrl, hostname));
            return new CoreServiceClient(basicHttpBinding, remoteAddress); ;
        }

        public void Dispose()
        {
            if (_client.State == CommunicationState.Faulted)
            {
                _client.Abort();
            }
            else
            {
                _client.Close();
            }
        }

        #region Core Service calls
        // Calls to the Core Service client library. Extend the Core service calls as needed.

        public UserData GetCurrentUser()
        {
            return _client.GetCurrentUser();
        }

        public IdentifiableObjectData Read(string id, ReadOptions readOptions)
        {
            return _client.Read(id, readOptions);
        }

        public IdentifiableObjectData Create(IdentifiableObjectData data)
        {
            return _client.Create(data, new ReadOptions());
        }


        public IdentifiableObjectData Update(IdentifiableObjectData data)
        {
            return _client.Update(data, new ReadOptions());
        }

        public IdentifiableObjectData[] GetSearchResults(SystemWideListFilterData filter)
        {
            return _client.GetSystemWideList(filter);
        }

        #endregion Core Service calls


        public void CreateMultimediaComp(string file, CoreServiceSession session)
        {
           FileInfo fileInfo = new FileInfo(file);
            if (fileInfo.Exists)
            {
                //string mmType = GetMultiMediaType(fileInfo.Extension);
                string mmType = fileInfo.Extension;

                //create a string to hold the temporary location of the image to use later
                string tempLocation = "";

                AccessTokenData accessToken = _client.GetCurrentUser();

                Console.WriteLine("Multimedia file is being uploaded in Tridion....." );
                //use the StreamUploadClient to upload the image into Tridion
                using (StreamUploadClient streamClient = new StreamUploadClient("streamUpload_basicHttp_201603"))//Endpoint name should match in the appsetting
                {
                    FileStream objfilestream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read);
                    tempLocation = streamClient.UploadBinaryContent(accessToken, objfilestream);
                }

                if (mmType != null)
                {
                    BinaryContentData bcd = new BinaryContentData
                    {
                        UploadFromFile = tempLocation,
                        MultimediaType = new LinkToMultimediaTypeData { IdRef = GetMultimediaTypeId(mmType) },
                        Filename = fileInfo.Name.ToLower(),
                        IsExternal = false                    
                    };

                    ComponentData compData = new ComponentData
                    {
                        LocationInfo = new LocationInfo
                        {
                            OrganizationalItem = new LinkToOrganizationalItemData
                            {
                                IdRef = ConfigurationManager.AppSettings["ImageFolder"]// Organizational item
                            },
                        },
                        ComponentType = ComponentType.Multimedia,
                        Title = fileInfo.Name.Split('.')[0],

                        Schema = new LinkToSchemaData
                        {
                            IdRef = ConfigurationManager.AppSettings["ImageSchema"]//Image Schema Id
                        },

                        IsBasedOnMandatorySchema = false,
                        IsBasedOnTridionWebSchema = true,
                        ApprovalStatus = new LinkToApprovalStatusData
                        {
                            IdRef = "tcm:0-0-0"
                        },
                        Id = "tcm:0-0-0",
                        BinaryContent = bcd  
                };

                    SchemaData sd = (SchemaData)session.Read(ConfigurationManager.AppSettings["ImageSchema"], new ReadOptions());
                    compData.Metadata = string.Format("<{0} xmlns=\"{1}\"/>", "Metadata", sd.NamespaceUri); // Metadata associated with Image Schema

                    ComponentData comp = (ComponentData)session.Create(compData);
                }
            }
        }

        private static string GetMultimediaTypeId(string fileExtension)
        {
            string tcmId = "";                       
            switch (fileExtension.TrimStart('.').ToLower())
            {
                case "jpg":
                case "jpeg":
                case "jpe":
                    tcmId = ConfigurationManager.AppSettings["JpegId"];
                    break;
                case "gif":
                    tcmId = ConfigurationManager.AppSettings["GifId"];
                    break;
                case "png":
                    tcmId = ConfigurationManager.AppSettings["PngId"];
                    break;
            }
         
          return tcmId;

        }


    }
}

