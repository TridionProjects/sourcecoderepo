using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using Tridion.ContentManager.CoreService.Client;

namespace ContentUploadToSDLTridion
{
    class Program
    {
        static void Main(string[] args)
        {
            var domaiName = ConfigurationManager.AppSettings["DomainName"];
            var userName = ConfigurationManager.AppSettings["UserName"];
            var password = ConfigurationManager.AppSettings["Password"];
            CoreServiceSession client = new CoreServiceSession(domaiName , userName , password);

            // Display the current user
            Console.WriteLine(client.GetCurrentUser().Description + " Welcome to the Upload Content Program!");

            XDocument doc = XDocument.Load("..\\..\\ExportedData\\export.xml");

            Console.WriteLine("...........Export Xml loaded ................");
            var data = doc.Descendants("RawProperty"); //Returns the collection of content with node name "RawProperty"


            var counter = 1;
            foreach (var eachitem in data)
            {
                //Checking if the value field is null or empty
                if (!string.IsNullOrEmpty(eachitem.Element("Value").Value))
                {
                    //Checking if the content is not Image type 
                    if (!eachitem.Element("Name").Value.Contains("Image"))
                    {
                        ComponentData comp = new ComponentData()
                        {
                            Id = "tcm:0-0-0",
                            Schema = new LinkToSchemaData { IdRef = ConfigurationManager.AppSettings["ComponentSchema"] },
                            Title = "New Component" + counter,
                            LocationInfo = new LocationInfo { OrganizationalItem = new LinkToOrganizationalItemData { IdRef = ConfigurationManager.AppSettings["ComponentFolder"] } }
                        };

                        // Get namespace from component schema
                        SchemaData sd = client.Read(ConfigurationManager.AppSettings["ComponentSchema"], null) as SchemaData;
                        string fields = string.Empty;
                        fields = eachitem.Element("Type").ToString() + eachitem.Element("Name") + eachitem.Element("Value");

                        comp.Content = string.Format("<{0} xmlns=\"{1}\">{2}</{0}>", sd.RootElementName, sd.NamespaceUri, fields);

                        ComponentData newComp = (ComponentData)client.Create(comp);// Component created 
                        Console.WriteLine("New component created : " + comp.Title);
                    }
                    else
                    {
                        //Creating Mulimedia Component
                        var path = ConfigurationManager.AppSettings["LocalImageFolder"]+ eachitem.Element("Value").Value;
                        client.CreateMultimediaComp(path,client);
                        Console.WriteLine("Multimedia component created" + counter);

                    }
                }
                counter++;
            }

            client.Dispose();

            Console.WriteLine("Content has been successfully uploaded . Press any key to exit.....");
            Console.ReadLine();

        }


    }
}
