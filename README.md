## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.

----------------------------------
## Upload Data Project

This project is a simple web console application which will upload the data extracted from any 3rd party CMS server(in xml format) into Tridion (in this case SDL Web 8.5) .
The exported data xml is placed inside the project folder named- "ExportedData" . 

----------------------------------
##Things to update for getting started with Upload 

Add references of the coreservice client dll from CM server , System.ServiceModel , etc.
Also, copy the endpoints from the coreservice client config to your app config/web config, etc.


Update the following configs with the respective values as per the environment and the endpoints (with correct domain ) that you have copied from CM server.(I am using SDL web 8.5)

    <add key="CoreServiceUrl" value="http://{0}/webservices/CoreService201603.svc/basicHttp"/>
    <add key="UserName" value="username"/>
    <add key="Password" value="password"/>
    <add key="DomainName" value="somedomain.com"/>
    <add key="ComponentSchema" value="tcm:xx-xxxx-x"/>
    <add key="ComponentFolder" value="tcm:xx-xxxx-x"/>
    <add key="ImageFolder" value="tcm:xx-xxxx-x"/>
    <add key="ImageSchema" value="tcm:xx-xxxx-x"/>
    <add key="LocalImageFolder" value="D:\binary\"/> 
    <add key="JpegId" value="tcm:xx-xxxx-x"/>
    <add key="PngId" value="tcm:xx-xxxx-x"/>
    <add key="GifId" value="tcm:xx-xxxx-x"/>	
	
----------------------------------